FROM gitpod/workspace-full

ARG GCS_DIR=/home/gitpod/google-cloud-sdk
ENV PATH=$GCS_DIR/bin:$PATH
RUN mkdir $GCS_DIR \
    && curl -fsSL https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-334.0.0-linux-x86_64.tar.gz \
    | tar -xzvC /home/gitpod \
    && $GCS_DIR/install.sh --quiet --usage-reporting=false --bash-completion=true \
    --additional-components alpha beta

COPY requirements.txt /home/gitpod/requirements.txt

RUN pip install -r /home/gitpod/requirements.txt

RUN echo "alias ltr='ls -lhtr'" >> /home/gitpod/.bashrc
