FROM python:3.8 as backend-build

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt && \
    rm /tmp/requirements.txt

FROM node:lts-alpine as frontend-build

WORKDIR /app
COPY ./svelte/package*.json ./
RUN npm install --frozen-lockfile 
COPY ./svelte .
RUN npm run build

FROM backend-build

COPY ./cbbstats /usr/local/cbbstats/cbbstats
COPY ./models/graph.pt /models/graph.pt
COPY --from=frontend-build /app/build /frontend
ENV PYTHONPATH "$PYTHONPATH:/usr/local/cbbstats"
CMD ["uvicorn", "--host=0.0.0.0", "cbbstats.api:app"]
