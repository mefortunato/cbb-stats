import datetime
import os
from joblib import load
from typing import Optional

import dgl
import httpx
import numpy as np
import pandas as pd
import requests
import torch
import tqdm
from bs4 import BeautifulSoup
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from google.cloud import firestore

from ..data.download import get_gamelogs, download_years, check_for_games, scrape_day, db_games_exist, games_on_day
from ..data.graph_dataset import GraphDataset
from ..models.graph import GraphModel, predict_games


db = firestore.Client()

THE_ODDS_API_KEY = os.environ.get("THE_ODDS_API_KEY", "s3cr3t")
SPORT_RADAR_API_KEY = os.environ.get("SPORT_RADAR_API_KEY", "s3cr3t")

headers = {
    "User-Agent": "Mozilla/5.0 (X11; CrOS x86_64 13597.94.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.186 Safari/537.36"
}

current_dir = os.path.dirname(__file__)

team_stats = pd.read_json(f'{current_dir}/2021.team-stats.series.json.gz')
model = torch.load(f'{current_dir}/first-try.pt')
scaler = load(f'{current_dir}/first-try.scaler.bin')

graph_model = torch.load(
    os.path.join(
        os.environ.get('MODEL_DIR', '/models'),
        'graph.pt'
    )
)


def format_game(game, teams):
    game_json = {"teams": [], "ranks": [], "scores": [], "team_ids": []}
    game = game.rename(columns={0: "team", 1: "score", 2: "time"}).fillna(0)
    for name in game["team"].iloc[:2]:
        if "(" in name and name != "Miami (FL)":
            split = name.split()
            team_name = " ".join(split[:-1])
            game_json["teams"].append(team_name)
            game_json["team_ids"].append(teams.get(team_name))
            game_json["ranks"].append(split[-1][1:-1])
        else:
            game_json["teams"].append(name)
            game_json["team_ids"].append(teams.get(name))
            game_json["ranks"].append(None)
    game_json["scores"] = game["score"].tolist()[:2]
    game_json["time"] = game.iloc[1]["time"]
    if len(game) == 3:
        game_json["extra"] = game.iloc[2]["team"]
    return game_json


app = FastAPI()


@app.get("/predict")
def predict(team1: str, team2: str):
    stats = np.hstack([team_stats.loc[team1].values,
                       team_stats.loc[team2].values])
    stats = scaler.transform(stats.reshape(1, -1))
    return float(model(torch.tensor(stats, dtype=torch.float)).detach().numpy().reshape(-1)[0])


@app.get("/games")
def get_games(day: int, month: int, year: int):
    url = f"https://www.sports-reference.com/cbb/boxscores/index.cgi?day={day}&month={month}&year={year}"
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, "lxml")
    tables = soup.findAll("table")
    if not tables:
        return []
    teams = {}
    for table in tables:
        for tr in table.findAll("tr"):
            tds = tr.findAll("td")
            for td in tds:
                try:
                    team_id = td.find("a")["href"].split("/")[-2]
                    team_name = td.find("a").text
                    teams[team_name] = team_id
                except:
                    pass
    games = [format_game(game, teams) for game in pd.read_html(resp.text)]
    return games


@app.get("/spreads")
def get_spreads(day: int, month: int, year: int):
    url = f"https://classic.sportsbookreview.com/betting-odds/ncaa-basketball/?date={year}{month:02}{day:02}"
    resp = requests.get(url, headers=headers)
    soup = BeautifulSoup(resp.text, "lxml")
    rows = soup.find_all("div", class_="event-holder")
    results = []
    for row in rows:
        team_div = row.find("div", {"class": "el-div eventLine-team"})
        line_div = row.find("div", {"rel": "43"})  # 43 for betway
        if not team_div or not line_div:
            print(row)
            continue
        teams = [a.text for a in team_div.find_all("a")]
        lines_str = [
            d.text.replace("PK", "0 ").replace("½", ".5").replace("\xa0", " ")
            for d in line_div.find_all("div", {"class": "eventLine-book-value"})
        ]
        lines = []
        odds = []
        for line_str in lines_str:
            if not line_str:
                continue
            line_split = line_str.split()
            lines.append(line_split[0])
            odds.append(line_split[1])
        results.append({"teams": teams, "lines": lines, "odds": odds})
    return results


@app.post("/db/odds")
def post_db_odds():
    today = datetime.date.today()
    url = f"https://api.sportradar.us/oddscomparison-ust1/en/eu/sports/sr:sport:2/{today.isoformat()}/schedule.json?api_key={SPORT_RADAR_API_KEY}"
    resp = httpx.get(url, timeout=None)
    resp.raise_for_status()
    events = resp.json()['sport_events']
    for event in events:
        db.collection('odds').document(event['id']).set(event)

    return len(events)


@app.get("/db/spreads")
def get_db_spreads(day: int, month: int, year: int):
    date_str = f'{year}-{month:02}-{day:02}'
    spreads = pd.DataFrame([
        doc.to_dict()
        for doc in db.collection('spreads').where(
            'commence_time', '>=', date_str
        ).stream()
    ])
    spreads = spreads[spreads['commence_time'].str.startswith(date_str)]
    spreads = spreads.sort_values('datetime', ascending=False)
    spreads = spreads.drop_duplicates('game_id')
    return spreads.to_dict(orient='records')


@app.post("/db/spreads")
def post_db_spreads():
    sport = "basketball_ncaab"
    regions = "us"
    markets = "spreads"
    resp = requests.get(
        f'https://api.the-odds-api.com/v4/sports/{sport}/odds/?apiKey={THE_ODDS_API_KEY}&regions={regions}&markets={markets}')
    spreads = resp.json()

    now = datetime.datetime.now()

    for spread in tqdm.tqdm(spreads):
        spread['game_id'] = spread.pop('id')
        spread['datetime'] = now
        db.collection('spreads').add(spread)

    return len(spreads)


@app.post("/db/games")
def post_db_games(day: int = None, month: int = None, year: int = None):
    if day is None:
        yesterday = datetime.datetime.today() - datetime.timedelta(days=1)
        day, month, year = yesterday.day, yesterday.month, yesterday.year
    print(day, month, year)
    if not db_games_exist(day, month, year) and check_for_games(day, month, year):
        df = scrape_day(day, month, year)
        for game in tqdm.tqdm(df.to_dict(orient='records')):
            db.collection('games').add(game)
        return len(df)
    else:
        return 0


# @app.get("/db/games")
def get_db_games(day: int, month: int, year: int):
    date_str = f'{year}-{month:02}-{day:02}'
    games = pd.DataFrame([
        doc.to_dict()
        for doc in db.collection('games').where(
            'DATE', '<=', date_str
        ).stream()
    ])
    games = games.to_dict(orient='records')
    return games


@app.get("/predict/graph")
def get_predict_graph(day: int, month: int, year: int):
    yesterday = datetime.date(year, month, day) - datetime.timedelta(days=1)
    games = pd.DataFrame(get_db_games(day, month, year))
    hist_games = games[games['DATE'] < f"{year}-{month}-{day:02}"]
    pred_games = games[games['DATE'] == f"{year}-{month}-{day:02}"]
    if len(pred_games) == 0:
        pred_teams = games_on_day(day, month, year)
        scores = False
    else:
        pred_teams = pred_games.loc[
            pred_games['HOME_AWAY'] == 'H',
            ['TEAM', 'OPP_TEAM']
        ].values.tolist()
        scores = True
    preds = predict_games(graph_model, hist_games, pred_teams)
    if scores:
        for pred in preds:
            game = pred_games[pred_games['TEAM'] == pred['team']].iloc[0]
            pred['score'] = game['PTS'] - game['OPP_PTS']
    return preds


@app.get("/stats/team")
def get_team_stats(team: str, year: int):
    df = get_gamelogs(team, year)
    df = df.mean().drop(["G", "SEASON"]).astype(float)
    return df.to_dict()


STATIC_DIR = os.environ.get('STATIC_DIR', '/frontend')
app.mount("/", StaticFiles(directory=STATIC_DIR, html=True), name="frontend")
