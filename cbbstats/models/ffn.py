import numpy as np
import torch
import torch.nn.functional as F
import tqdm
from typing import List

from ..data import columns

class FFN(torch.nn.Module):
    def __init__(
        self, 
        input_dim: int = len(columns.MODEL_INPUTS), 
        hidden_sizes: List[int] = [300, 300, 300], 
        output_dim: int = 1, 
        dropout: float = 0.2
    ):
        super(FFN, self).__init__()

        self.layers = torch.nn.ModuleList()
        for n, size in enumerate(hidden_sizes):
            layer_input_size = input_dim if n == 0 else hidden_sizes[n-1]
            self.layers.append(torch.nn.Linear(layer_input_size, size))

        self.output = torch.nn.Linear(hidden_sizes[-1], output_dim)

        self.dropout = torch.nn.Dropout(dropout)


    def forward(self, x: torch.tensor):
        for n, layer in enumerate(self.layers):
            x = layer(x)
            if n != len(self.layers) - 1:
                x = F.relu(x)
                x = self.dropout(x)
        x = self.output(x)
        return x

    def fit(
        self, 
        loader: torch.utils.data.DataLoader, 
        epochs: int, 
        validation_data: torch.utils.data.DataLoader, 
        loss_fn: torch.nn.Module, 
        optimizer: torch.optim.Optimizer
    ):
        for epoch in range(epochs):
            epoch_loss = 0.0
            n_samples = 0
            tqdm_loader = tqdm.tqdm(loader)
            self.train()
            for x, y in tqdm_loader:
                optimizer.zero_grad()
                output = self(x)
                loss = loss_fn(output, y)
                loss.backward()
                optimizer.step()
                epoch_loss += loss.item()*len(x)
                n_samples += len(x)
                tqdm_loader.set_description(
                    f'Epoch {epoch + 1} Loss: {round(epoch_loss/n_samples, 3)}'
                )

            val_epoch_loss = 0.0
            val_n_samples = 0
            val_tqdm_loader = tqdm.tqdm(validation_data)
            self.eval()
            for x, y in val_tqdm_loader:
                output = self(x)
                val_loss = loss_fn(output, y)
                val_epoch_loss += val_loss.item()*len(x)
                val_n_samples += len(x)
                val_tqdm_loader.set_description(
                    f'Epoch {epoch + 1} Validation Loss: {round(val_epoch_loss/val_n_samples, 3)}'
                )

    def predict(self, loader):
        preds = []
        tqdm_loader = tqdm.tqdm(loader)
        for x, y in tqdm_loader:
            output = self(x)
            preds.extend(output.detach().numpy())
        return np.array(preds)
        