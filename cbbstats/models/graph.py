import dgl
import torch

from ..data.graph_dataset import GraphDataset


class GraphModel(torch.nn.Module):
    def __init__(self, in_feat, hidden_feat, depth=3, dropout=0.1):
        super().__init__()
        self.w_i = torch.nn.Linear(in_feat, hidden_feat, bias=False)
        self.w_h = torch.nn.Linear(hidden_feat, hidden_feat, bias=False)
        self.w_o = torch.nn.Linear(in_feat + hidden_feat, hidden_feat)
        self.depth = depth
        self.W1 = torch.nn.Linear(hidden_feat * 2 + 2, hidden_feat)
        self.W2 = torch.nn.Linear(hidden_feat, 1)
        self.dropout = torch.nn.Dropout(p=dropout)

    def edge_to_node(self, graph, efeat, norm=True):
        with graph.local_scope():
            graph.edata['h'] = efeat
            if norm:
                reduce_fn = dgl.function.mean
            else:
                reduce_fn = dgl.function.sum
            graph.update_all(
                message_func=dgl.function.copy_e('h', 'e'),
                reduce_func=reduce_fn('e', 'h')
            )
            return graph.ndata['h']

    def message(self, graph, efeat):
        src, dst = graph.edges()
        node_state = self.edge_to_node(graph, efeat, norm=False)
        return (node_state[src] - efeat[dst]) / graph.in_degrees()[src].reshape(-1, 1)

    def apply_edges(self, edges):
        h = torch.cat([edges.src['h'], edges.dst['h'], edges.data['home']], 1)
        return {'pred': self.W2(torch.nn.functional.relu(self.W1(h)))}

    def pred(self, graph, nfeat):
        with graph.local_scope():
            graph.ndata['h'] = nfeat
            graph.apply_edges(self.apply_edges)
            return graph.edata['pred']

    def forward(self, graph, efeat, pgraph):
        edge_init = self.w_i(efeat)
        node_init = self.edge_to_node(graph, efeat)
        edge_message = torch.nn.functional.relu(edge_init)
        for _ in range(self.depth - 1):
            edge_message = self.message(graph, edge_message)
            edge_message = self.w_h(edge_message)
            edge_message = torch.nn.functional.relu(edge_init + edge_message)
            edge_message = self.dropout(edge_message)
        node_message = self.edge_to_node(graph, edge_message)
        node_message = torch.cat([node_init, node_message], dim=1)
        node_hidden = self.w_o(node_message)
        node_hidden = torch.nn.functional.relu(node_hidden)
        node_hidden = self.dropout(node_hidden)
        return self.pred(pgraph, node_hidden)

    def predict(self, games_df):
        if not hasattr(self, 'feature_scaler') or not hasattr(self, 'target_scaler'):
            raise ValueError(
                'model does not have feature or target scaler defined')
        ds = GraphDataset(games_df, feature_scaler=self.feature_scaler,
                          target_scaler=self.target_scaler, cache=False)
        hgraph, pgraph = ds.get_last()
        preds = self(hgraph, hgraph.edata['feat'], pgraph)
        src, dst = pgraph.edges()
        preds = self.target_scaler.inverse_transform(preds.detach())
        results = []
        for p, s, d in zip(preds, src, dst):
            results.append({
                'team': ds.team_name_map[s.item()],
                'opp': ds.team_name_map[d.item()],
                'pred': p
            })
        return results


def predict_games(model, historical_games, p_games, home_away=None):
    ds = GraphDataset(
        historical_games, feature_scaler=model.feature_scaler,
        target_scaler=model.target_scaler, cache=False
    )
    hgraph, pgraph = ds.generate_graphs(-1, get_all=True)
    team_idx = [[ds.team_idx_map[team[1]], ds.team_idx_map[team[0]]]
                for team in p_games]
    pgraph = dgl.graph(team_idx, num_nodes=hgraph.num_nodes())
    if home_away is None:
        pgraph.edata['home'] = torch.cat(
            [
                torch.ones((pgraph.num_edges(), 1)),
                torch.zeros((pgraph.num_edges(), 1))
            ], dim=1
        )
    preds = model(hgraph, hgraph.edata['feat'], pgraph)
    src, dst = pgraph.edges()
    preds = model.target_scaler.inverse_transform(preds.detach())
    results = []
    for p, s, d in zip(preds, src, dst):
        results.append({
            'team': ds.team_name_map[s.item()],
            'opp': ds.team_name_map[d.item()],
            'pred': p.item()
        })
    return results
