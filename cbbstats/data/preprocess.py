import pandas as pd

from . import columns

def rolling_df(df: pd.DataFrame, window: int = 5) -> pd.DataFrame:
    rolling_stats = df.sort_values(columns.DATE).groupby([columns.TEAM, columns.SEASON]).rolling(window).mean(skipna=True)[columns.INPUTS].shift(1).reset_index((0, 1))
    rolling_stats[columns.DATE] = df[columns.DATE]
    rolling_stats[columns.OPP_TEAM] = df[columns.OPP_TEAM]
    rolling_stats = rolling_stats.dropna(subset=columns.INPUTS, how='all', axis=0).fillna(0.0)
    return rolling_stats[columns.INPUTS]

def expanding_df(df: pd.DataFrame) -> pd.DataFrame:
    expanding_stats = df.sort_values(columns.DATE).groupby([columns.TEAM, columns.SEASON]).expanding().mean(skipna=True)[columns.INPUTS].shift(1).reset_index((0, 1))
    expanding_stats[columns.DATE] = df[columns.DATE]
    expanding_stats[columns.OPP_TEAM] = df[columns.OPP_TEAM]
    expanding_stats = expanding_stats.dropna(subset=columns.INPUTS, how='all', axis=0).fillna(0.0)
    return expanding_stats[columns.INPUTS]

def preprocess_df(df: pd.DataFrame, rolling_window: int = 5) -> pd.DataFrame:
    rolling = rolling_df(df, rolling_window)
    expanding = expanding_df(df)
    avgs = pd.merge(rolling, expanding, left_index=True, right_index=True, suffixes=("_ROLLING", "_EXPANDING"))
    res = pd.merge(df.drop(columns.INPUTS, axis=1), avgs, left_index=True, right_index=True, suffixes = ("", ""))
    res['y'] = df[columns.POINTS] - df[columns.OPP_POINTS]
    return res