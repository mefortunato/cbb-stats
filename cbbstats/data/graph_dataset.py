import dgl
import numpy as np
import torch
import tqdm
from sklearn.preprocessing import StandardScaler

from . import columns


def home_away(value):
    if value == 'H':
        return [1, 0]
    elif value == 'A':
        return [0, 0]
    else:
        return [0, 1]


class GraphDataset(dgl.data.DGLDataset):
    def __init__(self, df, feature_scaler=None, target_scaler=None, cache=True):
        self.data = df.copy()
        self.data['DATE'] = self.data['DATE'].astype(str)

        self.data['y'] = self.data[columns.POINTS] - \
            self.data[columns.OPP_POINTS]
        self.data['y'] = self.data[columns.POINTS] - \
            self.data[columns.OPP_POINTS]
        self.data = self.data.dropna(subset=['OPP_TEAM'])

        self.team_idx_map = {
            team: n
            for n, team in enumerate(np.unique(self.data[['TEAM', 'OPP_TEAM']].values))
        }
        self.team_name_map = {
            n: team
            for team, n in self.team_idx_map.items()
        }
        self.data.loc[:, 'u'] = self.data['TEAM'].map(self.team_idx_map)
        self.data.loc[:, 'v'] = self.data['OPP_TEAM'].map(self.team_idx_map)

        if feature_scaler is None:
            self.feature_scaler = StandardScaler()
            self.feature_scaler.fit(self.data[columns.GRAPH_STATS].values)
        else:
            self.feature_scaler = feature_scaler
        self.data[columns.GRAPH_STATS] = self.feature_scaler.transform(
            self.data[columns.GRAPH_STATS].values)

        if target_scaler is None:
            self.target_scaler = StandardScaler()
            self.target_scaler.fit(self.data[['y']].values)
        else:
            self.target_scaler = target_scaler
        self.data.loc[:, ['y']] = self.target_scaler.transform(
            self.data[['y']].values)

        self.data.loc[:, columns.GRAPH_STATS] = self.data[columns.GRAPH_STATS].fillna(
            0.0)

        self.dates = np.sort(self.data['DATE'].unique())[1:]

        self.cache = cache
        if self.cache:
            self.graphs = [
                self.generate_graphs(idx) for idx in tqdm.tqdm(range(len(self.dates)))
            ]

    def get_last(self):
        return self[self.dates.argmax()]

    def __len__(self):
        return len(self.dates)

    def generate_graphs(self, idx, get_all=False):
        date = self.dates[idx]
        if get_all:
            historical_df = self.data[self.data['DATE'] <= date]
        else:
            historical_df = self.data[self.data['DATE'] < date]
        historical_df = historical_df[
            historical_df['SEASON'] == historical_df['SEASON'].max()
        ]
        predict_df = self.data[self.data['DATE'] == date]
        historical_graph = dgl.graph(
            (historical_df['u'].values, historical_df['v'].values), num_nodes=len(self.team_idx_map))
        historical_graph.edata['feat'] = torch.from_numpy(
            historical_df[columns.GRAPH_STATS].values.astype(np.float32)
        )
        historical_graph.edata['y'] = torch.from_numpy(
            historical_df[['y']].values.astype(np.float32)
        )
        historical_graph.edata['home'] = torch.from_numpy(
            np.vstack(historical_df['HOME_AWAY'].apply(home_away).values)
        )
        historical_graph.edata['feat'] = torch.cat([
            historical_graph.edata['feat'],
            historical_graph.edata['home']
        ], dim=1)
        predict_graph = dgl.graph(
            (predict_df['u'].values, predict_df['v'].values),
            num_nodes=len(self.team_idx_map)
        )
        predict_graph.edata['y'] = torch.from_numpy(
            predict_df[['y']].values.astype(np.float32)
        )

        predict_graph.edata['home'] = torch.from_numpy(
            np.vstack(predict_df['HOME_AWAY'].apply(home_away).values)
        )
        historical_graph = historical_graph.add_self_loop()
        return historical_graph, predict_graph

    def __getitem__(self, idx):
        if self.cache:
            return self.graphs[idx]
        else:
            return self.generate_graphs(idx)
