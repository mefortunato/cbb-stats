import json
import numpy as np
import pandas as pd
import requests
import tqdm
import urllib
from bs4 import BeautifulSoup
from google.cloud import firestore
from typing import List, Optional

from . import columns

db = firestore.Client()

columns_converters = {name: float for name in columns.STATS}
graph_columns_converters = {name: float for name in columns.GRAPH_STATS}


def check_for_games(day, month, year):
    url = f'https://www.sports-reference.com/cbb/boxscores/index.cgi?month={month}&day={day}&year={year}'
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, "lxml")
    return len(soup.find_all(attrs={'class': 'winner'})) == len(soup.find_all(attrs={'class': 'loser'}))


def db_games_exist(day, month, year):
    return bool(len(db.collection('games').where('DATE', '==', f'{year}-{month}-{day}').limit(1).get()))


def download_years(years: List[int], path: Optional[str] = None) -> pd.DataFrame:
    """
    Download game logs from `years` and save to disk.
    """
    if path is None:
        path = f"data/{min(years)}-{max(years)}_boxscores.json.gz"
    dfs = []
    for year in years:
        print(year)
        team_names = get_team_names(year)
        for team in tqdm.tqdm(team_names.values()):
            try:
                df = get_gamelogs(team, year)
            except ValueError:
                continue
            df["OPP_TEAM"] = df["OPP_TEAM"].map(team_names)
            if df is None:
                continue
            dfs.append(df)
    df = pd.concat(dfs)
    if path:
        df.reset_index(drop=True).to_json(
            path, orient="records", compression="gzip")
    return df


def get_team_names(year: int):
    """
    Download team names dictionary for year `year`
    """
    url = f"https://www.sports-reference.com/cbb/seasons/{year}-school-stats.html"
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, "lxml")
    table = soup.find("table")
    teams = {}
    for tr in table.findAll("tr"):
        tds = tr.findAll("td")
        for td in tds:
            try:
                team_id = td.find("a")["href"].split("/")[-2]
                team_name = td.find("a").text
                teams[team_name] = team_id
            except:
                pass
    return teams


def clean_basic_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Perform cleaning on a DataFrame of basic stats
    """
    game_data = df.droplevel(0, axis=1)[
        ["G", "Unnamed: 2_level_1", "Date", "Tm", "Opp"]
    ]
    game_data["OPP_TEAM"] = game_data["Opp"].iloc[:, 0]
    game_data = game_data.loc[:, ~game_data.columns.duplicated(keep="last")]
    game_data = game_data.rename(
        columns={"Tm": "PTS", "Opp": "OPP_PTS",
                 "Unnamed: 2_level_1": "HOME_AWAY"}
    )
    game_data["HOME_AWAY"] = game_data["HOME_AWAY"].fillna("H").replace({
        "@": "A"})
    return pd.concat(
        [game_data, df["School"], df["Opponent"].rename(
            columns=lambda x: f"OPP_{x}")],
        axis=1,
    )


def clean_advanced_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Perform cleaning on a DataFrame of advanced stats
    """
    df = df.droplevel(0, axis=1)
    df = df.rename(columns={"Unnamed: 2_level_1": "HOME_AWAY"})
    df["HOME_AWAY"] = df["HOME_AWAY"].fillna("H").replace({"@": "A"})
    return df


def get_basic_stats(team: str, year: int):
    """
    Download basic boxscore stats for team `team` in year `year`
    """
    url = f"https://www.sports-reference.com/cbb/schools/{team}/{year}-gamelogs.html"
    try:
        df = pd.read_html(url)[0]
    except urllib.error.HTTPError:
        return None
    df = clean_basic_df(df)
    mask = df["G"].astype(str).str.isnumeric() == True
    df = df[mask]
    df["TEAM"] = team
    return df.reset_index(drop=True)


def get_advanced_stats(team: str, year: int):
    """
    Download advanced boxscore stats for team `team` in year `year`
    """
    url = f"https://www.sports-reference.com/cbb/schools/{team}/{year}-gamelogs-advanced.html"
    try:
        df = pd.read_html(url)[0]
    except urllib.error.HTTPError:
        return None
    df = clean_advanced_df(df)
    mask = df["G"].astype(str).str.isnumeric() == True
    df = df[mask]
    df["TEAM"] = team
    return df.dropna(axis=1, how="all").reset_index(drop=True)


def get_gamelogs(team: str, year: int, date: str = None):
    """
    Download combined basic and advanced stats for team `team` from year `year`
    """
    basic = get_basic_stats(team, year)
    if basic is None:
        return None
    advanced = get_advanced_stats(team, year).drop(
        columns=["Opp", "Tm", "W/L"])
    df = pd.concat([basic, advanced], axis=1)
    df = df.loc[:, ~df.columns.duplicated()]
    df = df.astype(columns_converters)
    df["DATE"] = df.pop("Date")
    df["SEASON"] = year
    if date is not None:
        df = df[df['DATE'] < date]
    return df


def team_names(html):
    soup = BeautifulSoup(html, "lxml")
    anchors = soup.find_all('a', attrs={'itemprop': 'name'})

    def get_name(anchor):
        try:
            return anchor['href'].split('/')[-2]
        except KeyError:
            return None
    return list(map(get_name, anchors))


def clean_box_score(df):
    return df.droplevel(0, axis=1).drop(['Starters', 'MP'], axis=1).iloc[-1].to_frame().T.reset_index(drop=True)


def scrape_game(game_id):
    url = f'https://www.sports-reference.com/cbb/boxscores/{game_id}.html'
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, "lxml")
    away_name, home_name = team_names(resp.text)
    if away_name is None or home_name is None:
        return None
    away, home = list(map(clean_box_score, pd.read_html(
        resp.text, match='Basic Box Score Stats')))
    away['TEAM'] = away_name
    home['TEAM'] = home_name
    df = pd.concat([
        pd.concat([away, home.add_prefix('OPP_')], axis=1),
        pd.concat([home, away.add_prefix('OPP_')], axis=1)
    ])
    return df


def scrape_day(day: int, month: int, year: int, neutral=False):
    resp = requests.get(
        f'https://www.sports-reference.com/cbb/boxscores/index.cgi?month={month}&day={day}&year={year}')
    soup = BeautifulSoup(resp.text, "lxml")
    gamelinks = soup.find_all('td', attrs={'class': 'gamelink'})
    game_ids = [link.a['href'].split('/')[-1][:-5] for link in gamelinks]
    games = [scrape_game(game_id) for game_id in tqdm.tqdm(game_ids)]
    df = pd.concat(filter(lambda x: x is not None, games))
    df = df.astype(graph_columns_converters)
    df['DATE'] = f'{year}-{month}-{day:02}'
    df['SEASON'] = year + 1 if month > 6 else year
    if neutral:
        df['HOME_AWAY'] = 'N'
    else:
        df['HOME_AWAY'] = np.array(['H', 'A'] * (len(df)//2))
    return df


def team_id(anchor):
    if anchor.get('href') is not None:
        return anchor['href'].split('/')[-2]
    else:
        return None


def games_on_day(day: int, month: int, year: int):
    resp = requests.get(
        f'https://www.sports-reference.com/cbb/boxscores/index.cgi?month={month}&day={day}&year={year}')
    soup = BeautifulSoup(resp.text, "lxml")
    game_divs = soup.find_all('div', attrs={'class': 'game_summary'})
    game_divs = [div.find_all('a') for div in game_divs]
    teams = [[team_id(divs[0]), team_id(divs[-1])] for divs in game_divs]
    return list(filter(lambda x: None not in x, teams))
