STATS = [
    'PTS', 'OPP_PTS', 'FG', 'FGA',
    'FG%', '3P', '3PA', '3P%', 'FT', 'FTA', 'FT%', 'ORB', 'TRB', 'AST',
    'STL', 'BLK', 'TOV', 'PF', 'OPP_FG', 'OPP_FGA', 'OPP_FG%', 'OPP_3P',
    'OPP_3PA', 'OPP_3P%', 'OPP_FT', 'OPP_FTA', 'OPP_FT%', 'OPP_ORB',
    'OPP_TRB', 'OPP_AST', 'OPP_STL', 'OPP_BLK', 'OPP_TOV', 'OPP_PF',
    'ORtg', 'DRtg', 'Pace', 'FTr', '3PAr', 'TS%', 'TRB%', 'AST%', 'STL%',
    'BLK%', 'eFG%', 'TOV%', 'ORB%', 'FT/FGA', 'DRB%'
]

GRAPH_STATS = [
    'PTS', 'OPP_PTS', 'FG', 'FGA',
    'FG%', '3P', '3PA', '3P%', 'FT', 'FTA', 'FT%', 'ORB', 'TRB', 'AST',
    'STL', 'BLK', 'TOV', 'PF', 'OPP_FG', 'OPP_FGA', 'OPP_FG%', 'OPP_3P',
    'OPP_3PA', 'OPP_3P%', 'OPP_FT', 'OPP_FTA', 'OPP_FT%', 'OPP_ORB',
    'OPP_TRB', 'OPP_AST', 'OPP_STL', 'OPP_BLK', 'OPP_TOV', 'OPP_PF'
]

INPUTS = [
    'PTS', 'OPP_PTS', 'FG', 'FGA', 'FG%', '3P', '3PA', '3P%', 'FT', 'FTA', 'FT%', 'ORB',
    'TRB', 'AST', 'STL', 'BLK', 'TOV', 'PF', 'OPP_FG', 'OPP_FGA', 'OPP_FG%',
    'OPP_3P', 'OPP_3PA', 'OPP_3P%', 'OPP_FT', 'OPP_FTA', 'OPP_FT%', 'OPP_ORB',
    'OPP_TRB', 'OPP_AST', 'OPP_STL', 'OPP_BLK', 'OPP_TOV', 'OPP_PF',
    'ORtg', 'DRtg', 'Pace', 'FTr', '3PAr', 'TS%', 'TRB%', 'AST%', 'STL%',
    'BLK%', 'eFG%', 'TOV%', 'ORB%', 'FT/FGA', 'DRB%'
]

AVG_INPUTS = [f"{inp}_ROLLING" for inp in INPUTS] + \
    [f"{inp}_EXPANDING" for inp in INPUTS]

MODEL_INPUTS = AVG_INPUTS + [f"{inp}_OPP" for inp in AVG_INPUTS]

TEAM = 'TEAM'
OPP_TEAM = 'OPP_TEAM'
DATE = 'DATE'
SEASON = 'SEASON'
POINTS = 'PTS'
OPP_POINTS = 'OPP_PTS'
