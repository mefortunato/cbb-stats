import numpy as np
import pandas as pd
import torch
from sklearn.preprocessing import StandardScaler

from . import columns
from . import preprocess


class StatsDataset(torch.utils.data.Dataset):
    def __init__(self, stats, margins=None):
        self.stats = stats
        self.margins = margins

    def __len__(self):
        return len(self.stats)

    def __getitem__(self, idx):
        if self.margins is not None:
            return torch.tensor(self.stats[idx], dtype=torch.float), torch.tensor(self.margins[idx], dtype=torch.float)
        else:
            return torch.tensor(self.stats[idx], dtype=torch.float)


class PreprocessStatsDataset(torch.utils.data.Dataset):
    def __init__(self, stats, scaler=None):
        self.stats = stats

        self.stats = self.stats[~self.stats['OPP_TEAM'].isna()]
        self.stats = preprocess.preprocess_df(self.stats)

        self.scaler = scaler
        if self.scaler is None:
            self.scaler = StandardScaler()
            self.scaler.fit(self.stats[columns.AVG_INPUTS])
        self.stats[columns.AVG_INPUTS] = self.scaler.transform(self.stats[columns.AVG_INPUTS])

        opp_df = self.stats.rename(columns={'TEAM': 'OPP_TEAM', 'OPP_TEAM': 'TEAM'})

        self.stats = pd.merge(
            self.stats.set_index(['DATE', 'TEAM', 'OPP_TEAM']), 
            opp_df.set_index(['DATE', 'TEAM', 'OPP_TEAM']), 
            left_index=True, right_index=True, 
            suffixes=("", "_OPP")
        )
        
    def process_key(self, idx):
        row = self.stats.iloc[idx]
        x = torch.tensor(row[columns.MODEL_INPUTS].values.astype(np.float32))
        y = torch.tensor(row.y).float()
        return x, y

    def __len__(self):
        return len(self.stats)

    def __getitem__(self, idx):
        return self.process_key(idx)
