module.exports = {
  pages: {
    index: {
      entry: 'src/main.js',
      title: 'CBB Stats'
    }
  },
  devServer: {
    disableHostCheck: true
  }
}
