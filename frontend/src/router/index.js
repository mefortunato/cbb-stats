import {
  createRouter,
  createWebHistory
} from 'vue-router';
import About from '../views/About.vue';
import Home from '../views/Home.vue';
import Stats from '../views/Stats.vue';
import PageNotFound from '../views/PageNotFound.vue';

const routes = [{
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component: About,
  },
  {
    path: '/stats',
    name: 'Stats',
    component: Stats,
  },
  {
    path: '/:catchAll(.*)',
    name: 'Page not found',
    component: PageNotFound,
  }

];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
